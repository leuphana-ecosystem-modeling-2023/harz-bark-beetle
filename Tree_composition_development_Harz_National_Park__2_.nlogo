;........................................................;
;                  1. General Information                ;
;........................................................;

; This model shows the change in tree composition in Harz National Park
; influenced by climate change and bark beetle population development.
; @authors Isa Metzger <isa.metzger@stud.leuphana.de>
; Jona Kehagias <jona.m.kehagias@stud.leuphana.de>
; Linn Lembke <linn.k.lembke@stud.leuphana.de>
; @license Apache 2.0 https://www.apache.org/licenses/LICENSE-2.0.html
; @copyright 2023 Isa Metzger, Jona Kehagias, Linn Lembke

;........................................................;
;                  2. Globals and Variables              ;
;........................................................;


globals [
  treeCount
  eatenPatchesCount
  turtlesCount
  spruce-vitality-history
  beech-vitality-history
  average-spruce-vitality
  average-beech-vitality
  current-year
  current-month
  current-day
  mean-temperature
  min-temperature-for-activity
  min-temperature-for-hibernation
  hibernation-state?
  hibernation-die-off-done?
  newly-hatched-turtles
  spruces-total
  spruces-low
  beeches-total
]

turtles-own [
  energy
  age
]

patches-own [
  has-spruce?
  has-beech?
  has-deadwood?
  spruce-vitality
  beech-vitality
  tree-age
  elevation
]

;........................................................;
;              3. Setup Procedures                       ;
;........................................................;

;........................................................;
;              3.1 General Setup Procedures              ;
;........................................................;

to setup
  clear-all
  setup-patches
  setup-turtles
  set min-temperature-for-activity 9
  set eatenPatchesCount 0
  set turtlesCount 10
  set spruce-vitality-history []
  set beech-vitality-history []
  set current-year 2018
  set current-month 1
  set current-day 1
  set mean-temperature -0.9
  set climate-scenario "no-change"
  set planting-beeches false
  set show-turtles true
  set no-regrowth false
  set show-energy false
  set hibernation-state? false
  reset-ticks
end

;........................................................;
;              3.2 Patches Setup Procedures              ;
;........................................................;

to setup-patches
  setup-elevation

; Setup of tree composition

  let total-patches count patches
  let percentage-spruce 0.7
  let percentage-beech 0.2
  let percentage-deadwood 0.1

  let num-patches-spruce round (percentage-spruce * total-patches)
  let num-patches-beech round (percentage-beech * total-patches)
  let num-patches-deadwood round (percentage-deadwood * total-patches)

  ask patches [
    set has-spruce? false
    set has-beech? false
    set has-deadwood? false
  ]

; Setup of spruc p. (p. = populations)

  ask n-of num-patches-spruce patches with [not has-spruce?] [
  set has-spruce? true
  set pcolor 54
  ]

  let num-spruce-with-vitality count patches with [has-spruce? and spruce-vitality > 0]
  let remaining-spruce-patches num-patches-spruce - num-spruce-with-vitality

  ask n-of (round (0.30 * remaining-spruce-patches)) patches with [has-spruce? and spruce-vitality = 0] [
    set spruce-vitality 95
  ]
  ask n-of (round (0.40 * remaining-spruce-patches)) patches with [has-spruce? and spruce-vitality = 0] [
    set spruce-vitality 80
  ]
  ask n-of (round (0.20 * remaining-spruce-patches)) patches with [has-spruce? and spruce-vitality = 0] [
    set spruce-vitality 70
  ]
  ask n-of (round (0.10 * remaining-spruce-patches)) patches with [has-spruce? and spruce-vitality = 0] [
    set spruce-vitality 55
  ]

  ask patches with [has-spruce?] [
    set tree-age 50
  ]

  ask patches with [elevation = 800] [
  if has-spruce? [
    set spruce-vitality spruce-vitality + 20
    ]
  ]

; Setup of beech p.

  ask n-of num-patches-beech patches with [not has-spruce?] [
    set has-beech? true
    set pcolor 68
  ]

  let num-beech-with-vitality count patches with [has-beech? and beech-vitality > 0]
  let remaining-beech-patches num-patches-beech - num-beech-with-vitality

  ask n-of (round (0.19 * remaining-beech-patches)) patches with [has-beech? and beech-vitality = 0] [
    set beech-vitality 95
  ]
  ask n-of (round (0.42 * remaining-beech-patches)) patches with [has-beech? and beech-vitality = 0] [
    set beech-vitality 80
  ]
  ask n-of (round (0.23 * remaining-beech-patches)) patches with [has-beech? and beech-vitality = 0] [
    set beech-vitality 70
  ]
  ask n-of (round (0.16 * remaining-beech-patches)) patches with [has-beech? and beech-vitality = 0] [
    set beech-vitality 55
  ]

  ask patches with [has-beech?] [
    set tree-age (random 81 + 20)
  ]

  check-beeches-over-800

; Setup of deadwood p.

  ask n-of num-patches-deadwood patches with [(not has-spruce?) and (not has-beech?)] [
    set has-deadwood? true
    set pcolor 37
    set tree-age 0
  ]
end

; Setup of elevation

to setup-elevation
  let center-x 0
  let center-y 0
  let radius 70

  let patches-in-circle patches with [
    distancexy center-x center-y <= radius
  ]

  ask patches-in-circle [
    set elevation 800
  ]

  ask patches with [not member? self patches-in-circle] [
    set elevation 0
  ]
end

; Remove beech p. from elevation over 800 meters

to check-beeches-over-800
  ask patches with [has-beech?] [
    if elevation = 800 [
      set has-beech? false
      set has-spruce? true
      set pcolor 54
      set spruce-vitality 80
      set beech-vitality 0
    ]
  ]
end

;........................................................;
;               3.3 Turtles Setup Procedures             ;
;........................................................;

to setup-turtles
  create-turtles 10 [setxy random-xcor random-ycor]
  ask turtles [
    set color 32
    set shape "bug"
    set size 2
    set age 0
    set energy 6
  ]
end

;........................................................;
;                  4. Go Procedures                      ;
;........................................................;

;........................................................;
;                  4.1 General Go Procedures             ;
;........................................................;

to go
  if current-year >= 2100 [stop]
  update-calendar
  climate-impact-temperature
  storm-damage
  climate-impact-spruces
  climate-impact-beeches
  elevation-impact-spruces
  die-trees
  regrow-trees
  calculate-average-vitality
  change-treecolor
  deactivate-tree-age
  check-for-hibernation
  recreate-turtles
  update-turtle-visibility
  replant-beeches
  update-plot
  update-tick
end

to update-tick
  ask turtles [set age age + 1]  ;; 1 tick = 1 week
  if current-month = 12 and (ticks mod 4) = 0 [  ;; hibernation on 12/01/each year
    let turtles-to-die count turtles * 0.75  ;; 75 % turtles die in hibernation
    ask n-of turtles-to-die turtles [
      die
    ]
  ]
  ask turtles [update-label]
  tick
end

;........................................................;
;                 4.2 Patches Go Procedures              ;
;........................................................;

; Regulating regrowth of spruce p. and beech p.

to regrow-trees
  if not no-regrowth [
  ask patches with [has-deadwood?][
    if random-float 1 <= 0.01 [
      let regrowing-patch self
      let parent-patch one-of patches in-radius 5 with [has-beech? or has-spruce?]

      if [has-spruce?] of parent-patch and [tree-age >= 30] of parent-patch  [
        ask regrowing-patch [
          set has-spruce? true
          set has-deadwood? false
          set spruce-vitality get-spruce-vitality-for-scenario
          set tree-age 0
          set pcolor 37
        ]
      ]

      if [has-beech?] of parent-patch and [tree-age] of parent-patch >= 40 and [elevation] of parent-patch = 0 [
        ask regrowing-patch [
          set has-beech? true
          set has-deadwood? false
          set beech-vitality get-beech-vitality-for-scenario
          set tree-age 0
          set pcolor 37
        ]
      ]
    ]
  ]
  regrow-beeches-in-bright-environments
  ]
end

to regrow-beeches-in-bright-environments
  ask patches with [has-deadwood? and elevation = 0] [
    if (count patches in-radius 1 with [has-deadwood?] >= 3) [
      set has-beech? true
      set has-deadwood? false
      set pcolor 44
      set beech-vitality get-beech-vitality-for-scenario
      set tree-age 0
    ]
  ]
end

; Adjusting vitality for regrowing tree p. according to climate scenario

to-report get-spruce-vitality-for-scenario
  if climate-scenario = "no-change" [
    report 80 ]
  if climate-scenario = "RCP2.6" [
      report 75 ]
  if climate-scenario = "RCP8.5" [
      report 70 ]
end

to-report get-beech-vitality-for-scenario
  if climate-scenario = "no-change" or climate-scenario = "RCP2.6" [
    report 80 ]
  if climate-scenario = "RCP8.5" [
    report 75 ]
end

; Implementing different colors for younger tree p. (visualized by legend in interface)

to change-treecolor
  ask patches with [has-spruce? and tree-age > 3] [
    set pcolor 23
  ]
  ask patches with [has-beech? and tree-age > 3] [
    set pcolor 44
  ]
  ask patches with [has-spruce? and tree-age > 10] [
    set pcolor 54
  ]
  ask patches with [has-beech? and tree-age > 10] [
    set pcolor 68
  ]
end

; Regulating climate impact on spruce p.

to climate-impact-spruces
  if not (current-year >= 2050 and climate-scenario = "RCP8.5") [
  let target-temperature 7.5
  let temperature-difference (mean-temperature - target-temperature)
  ifelse temperature-difference <= 0 [
    let temperature-effect 5.5 - (0.5 * abs(temperature-difference))
    ask patches with [has-spruce?] [
      set spruce-vitality spruce-vitality + temperature-effect
      if spruce-vitality > 100 [
        set spruce-vitality 100
      ]
    ]
  ][
    let temperature-effect 5.5 - (0.6 * temperature-difference)
    ask patches with [has-spruce?] [
      set spruce-vitality spruce-vitality - temperature-effect
      if spruce-vitality < 0 [
        set spruce-vitality 0
      ]
    ]
  ]
  ]

  if current-year >= 2050 and climate-scenario = "RCP8.5" [
  let target-temperature 7.5
  let temperature-difference (mean-temperature - target-temperature)
  ifelse temperature-difference <= 0 [
    let temperature-effect 5.5 - (0.5 * abs(temperature-difference))
    ask patches with [has-spruce?] [
      set spruce-vitality spruce-vitality + temperature-effect
      if spruce-vitality > 100 [
        set spruce-vitality 100
      ]
    ]
  ][
    let temperature-effect 5.5 - (0.47 * temperature-difference)
    ask patches with [has-spruce?] [
      set spruce-vitality spruce-vitality - temperature-effect
      if spruce-vitality < 0 [
        set spruce-vitality 0
      ]
    ]
  ]
  ]
end

; Regulating climate impact on beech p.

to climate-impact-beeches
  let target-temperature 9.5
  let temperature-difference (mean-temperature - target-temperature)
  ifelse temperature-difference <= 0 [
    let temperature-effect 5.5 - (0.5 * abs(temperature-difference))
    ask patches with [has-beech?] [
      set beech-vitality beech-vitality + temperature-effect
      if beech-vitality > 100 [
        set beech-vitality 100
      ]
    ]
  ][
    let temperature-effect 5.5 - (0.6 * temperature-difference)
    ask patches with [has-beech?] [
      set beech-vitality beech-vitality - temperature-effect
      if beech-vitality < 0 [
        set beech-vitality 0
      ]
    ]
  ]
end

; Regulating elevation impact on spruce p. vitality

to elevation-impact-spruces
  ask patches with [has-spruce? and elevation = 800] [
    set spruce-vitality spruce-vitality + 1
  ]
end

; Letting spruc p. and beech p. with vitality <= 1 die

to die-trees
  ask patches with [has-spruce? and spruce-vitality <= 1] [
    set has-spruce? false
    set has-deadwood? true
    set pcolor 37
    set spruce-vitality 0
  ]
  ask patches with [has-beech? and beech-vitality <= 1] [
    set has-beech? false
    set has-deadwood? true
    set pcolor 37
    set beech-vitality 0
  ]
end

to deactivate-tree-age
  ask patches with [has-deadwood?] [
    set tree-age 0
  ]
end

; Regulating potential beech p. planting (management-switch in interface)

to replant-beeches
  if planting-beeches and ticks mod 52 = 0 [
    let total-patches count patches
    let deadwood-patches count patches with [has-deadwood? and elevation = 0]

    if deadwood-patches >= (total-patches * 0.02) [
      let num-beeches-to-plant (total-patches * 0.02)
      let patches-to-convert patches with [has-deadwood? and elevation = 0]

      repeat num-beeches-to-plant [
        let random-patch one-of patches-to-convert
        ask random-patch [
          set has-deadwood? false
          set has-beech? true
          set beech-vitality get-beech-vitality-for-scenario
          set tree-age 0
          set pcolor 44
        ]
        set patches-to-convert patches-to-convert with [self != random-patch]
      ]
    ]
  ]
end

;........................................................;
;                4.3 Turtles Go Procedures               ;
;........................................................;

; Regulation of hibernation/activity-time of bark beetle p.

to check-for-hibernation

  if mean-temperature < min-temperature-for-activity [
    set hibernation-state? true]
  if mean-temperature > min-temperature-for-activity [
    set hibernation-state? false]

  if not hibernation-state? [
    move-turtles
    eat-trees
    reproduce-turtles
    check-death-turtles
  ]
end

; Defining movement behavior of bark beetle p.

to move-turtles
  ask turtles [
    let current-patch patch-here
    let surrounding-patches patches in-radius 3
        right random 360
        forward 1
        set energy energy - 1
        if [has-spruce?] of current-patch [
          ask current-patch [
            if spruce-vitality > 0 [
             set spruce-vitality-history lput spruce-vitality spruce-vitality-history
          ]
        ]
      ]
    ]
end

; Defining rules for bark beetle p. feeding on spruce p.

to eat-trees
  ask turtles [
    let current-patch patch-here
      if [has-spruce?] of current-patch and [tree-age] of current-patch >= 30 [
        set spruce-vitality spruce-vitality - 30
        set energy energy + 1
        set eatenPatchesCount (eatenPatchesCount + 1)
      ]
  ]
end

; Defining reproduction of bark beetle p. with sexual maturity

to reproduce-turtles
  ask turtles
    with [age > 6][
      hatch 1 [
      set energy 6
      set age 0
      set turtlesCount (turtlesCount + 1)
    ]
  ]
end

; Regulating bark beetle p. death (random death, natural death, no food)

to check-death-turtles
  if count turtles < 500 [
    let percent-die-randomly 3
    let num-to-die-randomly round (percent-die-randomly / 100 * count turtles)
    ask n-of num-to-die-randomly turtles [die]]

   if count turtles > 500 [
    let percent-die-randomly 7
    let num-to-die-randomly round (percent-die-randomly / 100 * count turtles)
    ask n-of num-to-die-randomly turtles [die]]

  ask turtles [
    if energy <= 0 [die]
    if age = 104 [die]
  ]

  ask turtles [
  let current-patch patch-here
  if elevation = 800 and not any? patches in-radius 1 with [
      has-spruce? and tree-age >= 30] [die]
  if elevation = 0 and not any? patches in-radius 2 with [
      has-spruce? and tree-age >= 30] [die]
]
end


; Recreating bark beetle p. when no p. remain

to recreate-turtles
 if count turtles = 0 [
     create-turtles 3 [setxy random-xcor random-ycor]
      ask turtles [
      set color 32
      set shape "bug"
      set size 2
      set age 0
      set energy 6
    ]
  ]
end

;........................................................;
;               4.4 Calendar Procedures                  ;
;........................................................;

to update-calendar
  let days-per-month [31 28 31 30 31 30 31 31 30 31 30 31]
  let leap-year? is-leap-year current-year
  let days-in-month item (current-month - 1) days-per-month
  if current-month = 2 and leap-year? [
    set days-in-month 29
  ]
  set current-day current-day + 7
  while [current-day > days-in-month][
    set current-day current-day - days-in-month
    set current-month current-month + 1
    if current-month > 12 [
      set current-month 1
      set current-year current-year + 1
      set leap-year? is-leap-year current-year
    ]
    set days-in-month item (current-month - 1) days-per-month
    if current-month = 2 and leap-year? [
      set days-in-month 29
    ]
  ]
  if ticks mod 52 = 0 [
    ask patches with [has-spruce? or has-beech?][
      set tree-age tree-age + 1
    ]
  ]
end

to-report is-leap-year [year]
  report (year mod 4 = 0 and (year mod 100 != 0 or year mod 400 = 0))
end

;........................................................;
;             4.5 Climate and Weather                    ;
;........................................................;

; Coupling temperatures and climate scenarios

to climate-impact-temperature
  if (climate-scenario = "no-change") [
  let mean-temperature-by-month-nochange [ -0.9 -0.4 2.9 6.7 11.5 14.5 16.2 16.0 12.6 8.1 3.5 0.4 ]
  set mean-temperature item (current-month - 1) mean-temperature-by-month-nochange
  ]

  if (climate-scenario = "RCP2.6") and (current-year >= 2018 and current-year <= 2070) [
    let mean-temperature-by-month-RCP2.6-a+b [ 0.2 0.7 3.8 7.6 12.4 15.6 17.3 17.1 13.8 9.3 4.7 1.5 ]
    set mean-temperature item (current-month - 1) mean-temperature-by-month-RCP2.6-a+b
  ]

  if (climate-scenario = "RCP2.6") and (current-year >= 2071 and current-year <= 2100) [
    let mean-temperature-by-month-RCP2.6-c [ 0.3 0.8 3.9 7.7 12.5 15.7 17.4 17.2 13.8 9.3 4.7 1.6 ]
    set mean-temperature item (current-month - 1) mean-temperature-by-month-RCP2.6-c
  ]

  if (climate-scenario = "RCP8.5") and (current-year >= 2018 and current-year <= 2050) [
    let mean-temperature-by-month-RCP8.5-a [ 0.3 0.8 3.9 7.7 12.5 15.7 17.4 17.2 13.8 9.3 4.7 1.6 ]
    set mean-temperature item (current-month - 1) mean-temperature-by-month-RCP8.5-a
  ]

  if (climate-scenario = "RCP8.5") and (current-year >= 2051 and current-year <= 2070) [
    let mean-temperature-by-month-RCP8.5-b [ 1.9 2.4 5.0 8.8 13.6 17.2 18.9 18.7 15.7 11.2 6.6 3.2 ]
    set mean-temperature item (current-month - 1) mean-temperature-by-month-RCP8.5-b
  ]

  if (climate-scenario = "RCP8.5") and (current-year >= 2071 and current-year <= 2100) [
    let mean-temperature-by-month-RCP8.5-c [ 3.1 3.6 6.0 9.8 14.6 18.5 20.2 20.0 16.9 12.4 7.8 4.4 ]
    set mean-temperature item (current-month - 1) mean-temperature-by-month-RCP8.5-c
  ]
end

; Implementation of storms

to storm-damage
  if random-float 100 < 8.77 [
    let total-patches count patches with [has-spruce? and tree-age >= 30]
    let num-affected-patches round (0.001 * total-patches)  ; 0.1% of the affected patches
    let affected-patches patches with [has-spruce? and tree-age >= 30]
    let selected-patches n-of num-affected-patches affected-patches

    ask selected-patches [
      set has-spruce? false
      set has-deadwood? true
      set spruce-vitality 0
      set pcolor 37

      let neighbors-with-turtles patches in-radius 5 with [any? turtles-here]
      if any? neighbors-with-turtles [
        sprout 1 [
          set color 32
          set shape "bug"
          set size 2
          set age 0
          set energy 6
        ]
      ]
    ]
  ]
end

;........................................................;
;              5. Labels and Displays                    ;
;........................................................;

; Implementation of plot/monitor to visualize tree composition by percentages

to update-plot
  let total-patches count patches
  let patches-below-800 count patches with [elevation = 0]
  let total-beeches count patches with [has-beech?]
  let total-spruces count patches with [has-spruce?]
  let spruces-below-800 count patches with [has-spruce? and elevation = 0]

  set spruces-total (total-spruces / total-patches) * 100
  set spruces-low (spruces-below-800 / patches-below-800) * 100
  set beeches-total (total-beeches / patches-below-800) * 100

  set spruces-total precision spruces-total 2
  set spruces-low precision spruces-low 2
  set beeches-total precision beeches-total 2
end

; Implementation of switch to show/not show energy of bark beetle p.

to update-label
  if energy > 0 [
    set label energy
  ] ifelse show-energy [
    set label energy ] [
    set label "" ]
end

; Implementation of switch to show/not show bark beetle p.

to update-turtle-visibility
  if show-turtles [
    ask turtles [
      set hidden? false
    ]
  ]
  if not show-turtles [
    ask turtles [
      set hidden? true
    ]
  ]
end

; Calculation of average spruce- p./beech p.-vitality for visualization in plot

to calculate-average-vitality
  let spruce-patches patches with [has-spruce?]
  let beech-patches patches with [has-beech?]
  set average-spruce-vitality mean [spruce-vitality] of spruce-patches
  set average-beech-vitality mean [beech-vitality] of beech-patches
end
@#$#@#$#@
GRAPHICS-WINDOW
248
27
817
597
-1
-1
1.4
1
8
1
1
1
0
1
1
1
-200
200
-200
200
1
1
1
weeks
30.0

BUTTON
30
26
93
59
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
106
26
169
59
go
go\n
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
28
324
202
369
bark beetle population count
turtlesCount
17
1
11

MONITOR
28
436
187
481
eaten spruces populations
eatenPatchesCount
17
1
11

SWITCH
31
134
152
167
show-energy
show-energy
1
1
-1000

PLOT
848
27
1303
249
Tree Composition
Time
Trees
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"Spruce populations total" 1.0 0 -12087248 true "" "plot count patches with [has-spruce?]"
"Spruce populations < 800m" 1.0 0 -14439633 true "" "plot count patches with [has-spruce? and elevation = 0]"
"Beech populations" 1.0 0 -5509967 true "" "plot count patches with [has-beech?]"
"Young spruces populations" 1.0 0 -6995700 true "" "plot count patches with [has-spruce? and tree-age < 10]"
"Young beeches populations" 1.0 0 -4079321 true "" "plot count patches with [has-beech? and tree-age < 10]"
"Deadwood populations" 1.0 0 -3889007 true "" "plot count patches with [has-deadwood?]"

PLOT
849
432
1304
595
tree population vitality
time
vitality
0.0
100.0
0.0
110.0
true
false
"" ""
PENS
"spruces" 1.0 0 -13210332 true "" "plot average-spruce-vitality"
"beeches" 1.0 0 -5509967 true "" "plot average-beech-vitality"

MONITOR
28
494
113
539
NIL
current-year
17
1
11

MONITOR
122
494
219
539
NIL
current-month
17
1
11

MONITOR
28
551
147
596
NIL
mean-temperature
17
1
11

CHOOSER
29
266
167
311
climate-scenario
climate-scenario
"no-change" "RCP2.6" "RCP8.5"
0

SWITCH
30
90
149
123
show-turtles
show-turtles
0
1
-1000

SWITCH
30
177
149
210
no-regrowth
no-regrowth
1
1
-1000

SWITCH
30
220
173
253
planting-beeches
planting-beeches
1
1
-1000

PLOT
848
268
1156
417
Percentage trees
Time
Percent
0.0
10.0
0.0
100.0
true
true
"update-plot" ""
PENS
"Spruces total" 1.0 0 -12087248 true "" "plot spruces-total"
"Spruces < 800m" 1.0 0 -14439633 true "" "plot spruces-low"
"Beeches total" 1.0 0 -5509967 true "" "plot beeches-total"

MONITOR
1171
268
1283
313
Spruces total (%)
spruces-total
17
1
11

MONITOR
1172
371
1280
416
Beches total (%)
beeches-total
17
1
11

MONITOR
1171
319
1305
364
Spruces < 800m (%)
spruces-low
17
1
11

MONITOR
28
379
208
424
active bark beetle populations
count turtles
17
1
11

@#$#@#$#@
## WHAT IS IT?

The model is showing the expected development of spruce- and beech-treecover in Harz National Park in Germany from 2018 to 2100, connected to the development of bark beetle populations and changes in temperature, that are based on different climate change scenarios. 

Spruces are visualized by dark green patches (pcolor 54), beeches by light green patches (pcolor 68), dead trees by brown patches (pcolor 37). One tick represents one week. The tree composition of spruces and beeches in the model are based on data on the dominant tree species occurence from 2018. The circle in the center of the model represents the area of the National Park with an elevation over 800 meters, which is dominated by spruces.

## HOW IT WORKS


The overall behavior of the model is created by the following rules: 

**Influence bark beetles on spruces/beeches:** 
The turtles/beetle populations in the model move to represent flying and infestation of trees. Thereby, they loose energy. When a turtle moves and reaches a tree patch which represents a spruce population with an age >= 30 the turtle eats on the tree, which represents an infestation by an *I. typographus* population. Thereby the spruce population loses vitality, since an infestation weakens the tree, and the beetle population gains energy.

**Influence temperature on spruces/beeches:** 
The influence of temperature is species-specific, as the optimum annual mean temperature for spruce is between 5 and 7.5 degrees Celsius while the optimum for beech is 5.5 to 9.5 degrees Celsius. Higher monthly mean temperatures influences the vitality of the trees negatively. Lower monthly mean temperatures influence the vitality positively.

**Vitality:** 
The vitality of trees is influenced by temperature and bark beetle infestation. 

**Influence temperature on bark beetles:** 
The Bark beetle activity is temperature dependent. The beetles in the model are only able to move, eat and reproduce in the timeframe where the monthly mean temperatures resonate with the species’ minimum temperature for activity.  The minimum mean temperature for activity in the model is 9 degrees Celsius.

**Reproduction of Bark Beetles:** 
The beetle populations are able to reproduce, to represent their exponential growth. The turtles in the model can only reproduce after 6 ticks (= 6 weeks). They reproduce individually since turtles represent populations.

**Storms:** 
On 8.77 % of the days, a storm occurs. The consequences are spruce deaths  and an increase in bark beetle occurence.

## HOW TO USE IT

1. Press the SETUP button.
2. USe the default setting or adjust the switches and choosers for exploration (see Things To Try for explanation).
3. Press the GO button to start the simulation.
4. Look at the monitors on the left side to see the number of bark beetle populations (since the start of the model and currently active) as well as the date and current mean temperature.
5.Look at the plots and monitors on the right side to observe the tree changes in composition (total numbers above and percantages below) as well as the tree vitality fluctuationg over the seasons.

Notes: The model will stop in the beginning of the year 2100 as that marks the end of the period which should be modeled. It takes about 15 minutes to run the model completely.

## THINGS TO TRY

**climate-scenario**
RCPs (Representative Concentration Pathways) are a set of scenarios used in climate science to model future greenhouse gas concentrations in the earth's atmosphere. These scenarios are used to project potential future climatic changes based on different levels of greenhouse gas emissions. The RCP scenarios are defined by the concentration of greenhouse gases, primarily carbon dioxide (CO<sup>2</sup>), in the atmosphere by the year 2100. This model gives the opportunity to experiment with the climate scenarios RCP 2.6 (low greenhouse gas emissions pathway) and RCP 8.5 (continous-emission scenario).

**planting-beeches**
This switch enables the user to activate a management procedure for beech plantations, that has been conducted in Harz National Park for several years. If activated, beeches will grow on additional 2 % of the area every year.

**no-regrowth**
This switch eliminates the natural regrowing process of spruces and beeches, allowing for a clearer visualization of the numbers and patterns of infestation and death in tree populations.

**show-turtles and show-energy**
The show-turtles switch enables the user to deactivate turtle-visibility to follow feeding patterns with less visual disturbance. The show-energy switch enables the user to investigate turtle energies more in detail, for example for observing the reproduction process.

## THINGS TO NOTICE

While running the model, take a closer look at the following things: 

- How does the chosen climate scenario influence the tree composition development?
- How does the chosen climate scenario influence the bark beetle development, especially activity time?
- How does the chosen climate scenario impact the vitality of the two tree species?
- How is the development in the area with an elevation over 800 meters?

## EXTENDING THE MODEL

To make the model more spatially accurate, a background map with the dominant tree species’ placements could be useful. A basemap model can be found on https://gitlab.gwdg.de/leuphana-ecosystem-modeling-2023/harz-bark-beetle .

## CREDITS AND REFERENCES

**Sources of data that was used to construct the model is explained and referenced in this accompanying report:**
Kehagias, J., Lembke, L. and Metzger, I. (2020) Tree composition development in Harz National Park under the influence of bark beetle population development and climate change. Term Paper. Leuphana University Lueneburg.

**Authors:**
Isa Metzger ([isa.metzger@stud.leuphana.de](mailto:isa.metzger@stud.leuphana.de)) 
Jona Kehagias ([jona.m.kehagias@stud.leuphana.de](mailto:jona.m.kehagias@stud.leuphana.de))
Linn Lembke ([linn.k.lembke@stud.leuphana.de](mailto:linn.k.lembke@stud.leuphana.de))

**License:** Apache 2.0 https://www.apache.org/licenses/LICENSE-2.0.html

**Copyright:** 2023 Isa Metzger, Jona Kehagias, Linn Lembke
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.3.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
